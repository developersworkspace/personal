var gulp = require('gulp');
var gutil = require('gulp-util');
var minify = require('gulp-minifier');
var exec = require('child_process').exec;
var sftp = require('gulp-sftp');
var GulpSSH = require('gulp-ssh');
var handlebars = require('gulp-compile-handlebars');
var clean = require('gulp-clean');
var merge = require('merge-stream');

gulp.task('default', function () {
    return gutil.log('Gulp is running!');
});


gulp.task('prepare', ['deploy'], function () {

    var config = {
        host: '46.101.17.78',
        port: 22,
        username: 'root',
        password: 'MidericK96',
    };

    var ssh = new GulpSSH({
        ignoreErrors: false,
        sshConfig: config
    });

    return ssh.exec(['rm -rf /var/www/html']);
});

gulp.task('clean', function () {

    return gulp.src('destination')
        .pipe(clean());
});

gulp.task('deploy', ['clean'], function () {

    var options = {
        ignorePartials: true,
        batch: ['html/partials']
    }

    gulp.src([
        'html/**',
        '!html/**/*.html'
    ]).pipe(minify({
        minify: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        minifyJS: true,
        minifyCSS: true
    })).pipe(gulp.dest('destination'));

    gulp.src([
        'html/**/*.html'
    ]).pipe(handlebars(null, options)).pipe(minify({
        minify: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        minifyJS: true,
        minifyCSS: true
    })).pipe(gulp.dest('destination'));
});

gulp.task('publish', ['prepare'], function () {
    return gulp.src([
        'destination/**'
    ])
        .pipe(sftp({
            host: '46.101.17.78',
            user: 'root',
            pass: 'MidericK96',
            remotePath: '/var/www/html'
        }));
});

gulp.task('serve', ['deploy'], function (cb) {
    ///gulp.watch('html/**', ['serve']);

    exec('http-server -p 5000 destination', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});


